using Common.CsvParser;
using Models.Models;
using Repository.Service;
using Scraper.Constants;
using Scraper.Services;

namespace api.HostedServices
{
    public class ScraperWorker : BackgroundService
    {
        private readonly ILogger<ScraperWorker> _logger;
        private readonly IScraperService _scraperService;
        private readonly IRepositoryService _repositoryService;
        private readonly ICsvParser _csvParser;

        public ScraperWorker(ILogger<ScraperWorker> logger, IScraperService scraperService, IRepositoryService repositoryService, ICsvParser csvParser)
        {
            _scraperService = scraperService;
            _repositoryService = repositoryService;
            _csvParser = csvParser;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Scraper started running at: {time}", DateTimeOffset.Now);
                try
                {
                    await _repositoryService.CleanDatabaseAsync();

                    var filteredMonths = await ScrapeFilteredMonthsAsync();

                    await InsertNetworksAsync(filteredMonths);

                    if (await _repositoryService.InsertParsedDataEntriesAsync(filteredMonths))
                    {
                        _logger.LogInformation("Scraper successfully stopped running at: {time}", DateTimeOffset.Now);
                        break;
                    }
                    else
                    {
                        _logger.LogError("Error while scraping, cleaning database and retrying in 10 seconds");
                        await _repositoryService.CleanDatabaseAsync();

                        await Task.Delay(10000, stoppingToken);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        private async Task InsertNetworksAsync(List<CsvEntry> bothFilteredMonths)
        {
            var networkNames = _csvParser.GetNetworkNames(bothFilteredMonths);

            _logger.LogInformation("Inserting networks:", networkNames);

            await _repositoryService.InsertNetworksAsync(networkNames);
        }

        private async Task<List<CsvEntry>> ScrapeFilteredMonthsAsync()
        {
            var document = await _scraperService.GetDocumentAsync();

            var downloadLinks = _scraperService.GetDownloadLinksFromDocument(document);
            var bothFilteredMonths = new List<CsvEntry>();

            foreach (var downloadLink in downloadLinks)
            {
                var filtered = await GetFilteredEntries(downloadLink);

                if (filtered != null)
                    bothFilteredMonths.AddRange(filtered);
            }

            return bothFilteredMonths;
        }

        private async Task<List<CsvEntry>?> GetFilteredEntries(string downloadLink)
        {
            var stream = await _scraperService.GetCsvStream($"{ScraperConstants.BaseUrl}{downloadLink}");

            if (stream != null)
            {
                var entries = _csvParser.GetCsvEntriesFromStream(stream);

                var filtered = _csvParser.FilterButai(entries);

                return filtered;
            }

            return null;
        }
    }
}