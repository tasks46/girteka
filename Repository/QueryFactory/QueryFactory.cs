using Models.Models;
using Dapper;

namespace Repository.QueryFactory
{
    public static class QueryFactory
    {
        public static SqlBuilder.Template GenerateInsertNetworkQueryTemplate(string networkName)
        {
            var parameters = new
            {
                OBTName = networkName
            };

            var sqlBuilder = new SqlBuilder().AddParameters(parameters);
            var query = sqlBuilder.AddTemplate(Queries.Queries.InsertNetworkQuery);

            return query;
        }

        public static IList<string> GetSqlsInBatches(IList<CsvEntry> entries)
        {
            var batchSize = 1000;

            var sqlsToExecute = new List<string>();
            var numberOfBatches = (int)Math.Ceiling((double)entries.Count / batchSize);

            for (int i = 0; i < numberOfBatches; i++)
            {
                var entiresToInsert = entries.Skip(i * batchSize).Take(batchSize);

                var valuesToInsert = entiresToInsert.Select(entry =>
                    string.Format($@"((SELECT id from ""Networks"" n WHERE n.name = '{entry.NetworkName}'), '{entry.OBTName}',
                                    '{entry.OBJGVType}', '{entry.OBJNumber}', '{entry.PPlus}', '{entry.PLT}', '{entry.PMinus}')"));

                sqlsToExecute.Add(Queries.Queries.InsertParsedDataQuery + string.Join(',', valuesToInsert));
            }

            return sqlsToExecute;
        }

        public static SqlBuilder.Template GenerateRetrieveParsedDataQueryTemplate()
        {
            var sqlBuilder = new SqlBuilder();

            var query = sqlBuilder.AddTemplate(Queries.Queries.RetrieveParsedDataQuery);

            return query;
        }
    }
}