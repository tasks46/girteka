using Common.Constants;
using Models.Models;
using Dapper;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Npgsql;
using System.Transactions;

namespace Repository.Service
{
    public class RepositoryService : IRepositoryService
    {
        private readonly ILogger<RepositoryService> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IConfiguration _configuration;

        public RepositoryService(ILogger<RepositoryService> logger, IMemoryCache memoryCache, IConfiguration configuration)
        {
            _logger = logger;
            _memoryCache = memoryCache;
            _configuration = configuration;
        }

        public async Task InsertNetworksAsync(List<string> networkNames)
        {
            if (networkNames.Count <= 0)
                throw new ArgumentNullException(nameof(networkNames));

            await using var connection = new NpgsqlConnection(_configuration.GetConnectionString("TestDb"));
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            foreach (var networkName in networkNames)
            {
                try
                {
                    var query = QueryFactory.QueryFactory.GenerateInsertNetworkQueryTemplate(networkName);

                    await connection.QueryAsync<int>(query.RawSql, query.Parameters);
                }
                catch (NpgsqlException npgsqlException)
                {
                    scope.Dispose();
                    _logger.LogError(npgsqlException.Message);

                    throw;
                }
            }

            _logger.LogInformation("Networks added successfully");
            scope.Complete();
        }

        public async Task<bool> InsertParsedDataEntriesAsync(List<CsvEntry> csvEntries)
        {
            if (csvEntries.Count <= 0)
                throw new ArgumentNullException(nameof(csvEntries));

            await using var connection = new NpgsqlConnection(_configuration.GetConnectionString("TestDb"));
            connection.Open();

            var queries = QueryFactory.QueryFactory.GetSqlsInBatches(csvEntries);

            _logger.LogInformation($"Generated {queries.Count} SQL queries to insert parsed data");

            using (var transaction = connection.BeginTransaction())
            {
                for (int i = 0; i < queries.Count; i++)
                {
                    try
                    {
                        connection.Execute(queries[i], transaction);

                        if (i % 20 == 0)
                            _logger.LogInformation($"Executed {i}/{queries.Count} queries");
                    }
                    catch (NpgsqlException npgsqlException)
                    {
                        connection.Close();
                        _logger.LogError(npgsqlException.Message);

                        throw;
                    }
                }

                transaction.Commit();
            }
            connection.Close();

            SetupCache(csvEntries);

            return true;
        }

        public async Task<IEnumerable<CsvEntry>> RetrieveParsedDataEntriesAsync()
        {
            if (_memoryCache.TryGetValue(CommonConstants.CacheKey, out List<CsvEntry> cachedList))
            {
                _logger.LogInformation($"Found {cachedList.Count} entries in cache");

                return cachedList;
            }

            _logger.LogInformation($"Cache empty, retrieving parsed entries");

            await using var connection = new NpgsqlConnection(_configuration.GetConnectionString("TestDb"));

            try
            {
                await connection.OpenAsync();
                var query = QueryFactory.QueryFactory.GenerateRetrieveParsedDataQueryTemplate();

                var results = await connection.QueryAsync<CsvEntry>(query.RawSql);

                _logger.LogInformation($"Retrieved {results.Count()} parsed entries");

                if (results.Any())
                    SetupCache(results);

                return results;
            }
            catch (NpgsqlException npgsqlException)
            {
                _logger.LogError(npgsqlException.Message);

                throw;
            }
        }

        public async Task CleanDatabaseAsync()
        {
            await using var connection = new NpgsqlConnection(_configuration.GetConnectionString("TestDb"));

            try
            {
                await connection.OpenAsync();

                connection.Execute(@"DELETE FROM ""Parsed_data""");
                connection.Execute(@"DELETE FROM ""Networks""");

                _logger.LogInformation("Database cleaned");
            }
            catch (NpgsqlException npgsqlException)
            {
                _logger.LogError(npgsqlException.Message);

                throw;
            }
        }

        private void SetupCache(IEnumerable<CsvEntry> results)
        {
            var cacheExpiryOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(5),
                SlidingExpiration = TimeSpan.FromMinutes(3)
            };
            _memoryCache.Remove(CommonConstants.CacheKey);

            _logger.LogInformation($"Saving {results.Count()} entries into cache");

            _memoryCache.Set(CommonConstants.CacheKey, results, cacheExpiryOptions);
        }
    }
}