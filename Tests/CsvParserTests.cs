using Models.Models;

namespace Tests
{
    public class CsvParserTests
    {
        private readonly IScraperService _scraperService;
        private readonly ICsvParser _csvParser;

        public CsvParserTests()
        {
            var logger = Mock.Of<ILogger<ScraperService>>();

            _scraperService = new ScraperService(logger);
            _csvParser = new CsvParser();
        }

        [Test]
        public void ShouldFilterButai()
        {
            var butai = Enumerable.Repeat(new CsvEntry() { OBTName = CommonConstants.FilterBy }, 10).ToList();
            var namai = Enumerable.Repeat(new CsvEntry() { OBTName = "Namas" }, 10).ToList();

            var ulfiltered = butai.Concat(namai).ToList();

            var filtered = _csvParser.FilterButai(ulfiltered);

            Assert.That(filtered, Has.Count.EqualTo(10));
        }

        [Test]
        public void ShouldGetNetworkNames()
        {
            var csvEntries = new List<CsvEntry>();

            PopulateNetworkNames(csvEntries);

            var numberOfUniqueEntries = csvEntries.Count;

            PopulateNetworkNames(csvEntries);

            var parsedNames = _csvParser.GetNetworkNames(csvEntries);

            Assert.That(parsedNames, Is.Not.Null);
            Assert.That(parsedNames, Has.Count.EqualTo(numberOfUniqueEntries));
        }

        [Test]
        public async Task ShouldGetGetCsvEntriesFromStream()
        {
            var stream = await _scraperService.GetCsvStream(TestsConstants.CsvDownloadUrl);

            Assert.That(stream, Is.Not.Null);

            var entries = _csvParser.GetCsvEntriesFromStream(stream);

            Assert.That(entries, Is.Not.Null);
            Assert.That(entries, Is.Not.Empty);
        }

        private static void PopulateNetworkNames(List<CsvEntry> networkNames)
        {
            networkNames.Add(new CsvEntry() { NetworkName = "A" });
            networkNames.Add(new CsvEntry() { NetworkName = "B" });
            networkNames.Add(new CsvEntry() { NetworkName = "C" });
            networkNames.Add(new CsvEntry() { NetworkName = "D" });
            networkNames.Add(new CsvEntry() { NetworkName = "E" });
            networkNames.Add(new CsvEntry() { NetworkName = "F" });
            networkNames.Add(new CsvEntry() { NetworkName = "G" });
        }
    }
}