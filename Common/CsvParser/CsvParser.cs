using Common.Constants;
using Models.Models;
using CsvHelper;
using System.Globalization;

namespace Common.CsvParser
{
    public class CsvParser : ICsvParser
    {
        public List<CsvEntry> GetCsvEntriesFromStream(Stream csvStream)
        {
            if (csvStream == null)
                throw new ArgumentNullException(nameof(csvStream));

            var csvEntries = new List<CsvEntry>();

            using (var reader = new StreamReader(csvStream))
            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csvReader.Read();
                csvReader.ReadHeader();
                while (csvReader.Read())
                {
                    var record = csvReader.GetRecord<CsvEntry>();
                    csvEntries.Add(record);
                }
            }

            return csvEntries;
        }

        public List<CsvEntry> FilterButai(IList<CsvEntry> csvEntries)
        {
            var filtered = csvEntries.Where(property => property.OBTName.ToUpper() == CommonConstants.FilterBy);

            return filtered.ToList();
        }

        public List<string> GetNetworkNames(IList<CsvEntry> csvEntries)
        {
            var networkNames = csvEntries.GroupBy(entry => entry.NetworkName).Select(entry => entry.Key).Distinct();

            return networkNames.ToList();
        }
    }
}