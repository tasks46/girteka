using AngleSharp.Dom;

namespace Scraper.Services
{
    public interface IScraperService
    {
        Task<Stream> GetCsvStream(string url);

        Task<IDocument> GetDocumentAsync();

        List<string> GetDownloadLinksFromDocument(IDocument document);
    }
}