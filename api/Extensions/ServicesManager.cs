using Common.CsvParser;
using FluentMigrator.Runner;
using Repository;
using Repository.Context;
using Repository.Service;
using Scraper.Services;

namespace api.Extensions
{
    public static class ServicesManager
    {
        public static IServiceCollection RegisterServices(this IServiceCollection collection)
        {
            collection.AddSingleton<DapperContext>();
            collection.AddSingleton<Database>();

            collection.AddTransient<ICsvParser, CsvParser>();
            collection.AddTransient<IScraperService, ScraperService>();
            collection.AddTransient<IRepositoryService, RepositoryService>();

            return collection;
        }

        public static IServiceCollection RegisterLogging(this IServiceCollection collection, IConfiguration configuration)
        {
            var repositoryAssembly = AppDomain.CurrentDomain.GetAssemblies().SingleOrDefault(assembly => assembly.GetName().Name == "Repository");

            collection.AddLogging(c => c.AddFluentMigratorConsole())
                    .AddFluentMigratorCore()
                    .ConfigureRunner(c => c.AddPostgres11_0()
                        .WithGlobalConnectionString(configuration.GetConnectionString("TestDb"))
                        .ScanIn(repositoryAssembly).For.Migrations());

            return collection;
        }
    }
}