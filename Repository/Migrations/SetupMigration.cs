using FluentMigrator;

namespace Repository.Migrations
{
    [Migration(202208290001)]
    public class SetupMigration : Migration
    {
        public override void Down()
        {
            Delete.Table("Networks");
            Delete.Table("Parsed_data");
        }

        public override void Up()
        {
            Create.Table("Networks")
             .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
             .WithColumn("name").AsString(255).Unique().NotNullable();

            Create.Table("Parsed_data")
                .WithColumn("region_id").AsInt32().NotNullable().ForeignKey("Networks", "id")
                .WithColumn("obt_name").AsString(255).NotNullable()
                .WithColumn("obj_gv_type").AsString(255).NotNullable()
                .WithColumn("obj_number").AsString(255).NotNullable()
                .WithColumn("pplus").AsString(255).Nullable()
                .WithColumn("plt").AsString(255).NotNullable()
                .WithColumn("pminus").AsString(255).Nullable()
                ;

            Create.Index("ix_region").OnTable("Parsed_data").OnColumn("region_id").Ascending()
                .WithOptions().NonClustered();
        }
    }
}