using api.Extensions;
using api.HostedServices;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddMemoryCache();
builder.Services.RegisterServices();
builder.Services.RegisterLogging(builder.Configuration);
builder.Services.AddHostedService<ScraperWorker>();

var app = builder.Build();

app.MigrateDatabase();
app.UseHttpsRedirection();
app.MapEndpoints();
app.Run();