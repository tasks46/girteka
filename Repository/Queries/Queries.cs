namespace Repository.Queries
{
    public static class Queries
    {
        public const string InsertNetworkQuery = @"INSERT INTO ""Networks"" (name) VALUES(@OBTName)";

        public const string InsertParsedDataQuery = @"INSERT INTO ""Parsed_data"" (region_id,obt_name, obj_gv_type,obj_number,pplus,plt,pminus) VALUES ";

        public const string RetrieveParsedDataQuery = @"SELECT
                                                        network.name as NetworkName,
                                                        data.obt_name as OBTName,
                                                        data.obj_gv_type as OBJGVType,
                                                        data.obj_number as OBJNumber,
                                                        data.pplus,
                                                        data.plt,
                                                        data.pminus
                                                       FROM ""Parsed_data"" data
                                                       INNER JOIN ""Networks"" network on network.id = data.region_id";
    }
}