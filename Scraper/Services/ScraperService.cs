using AngleSharp;
using AngleSharp.Dom;
using Microsoft.Extensions.Logging;
using Scraper.Constants;
using Scraper.Helper;

namespace Scraper.Services
{
    public class ScraperService : IScraperService
    {
        private readonly ILogger<ScraperService> _logger;

        public ScraperService(ILogger<ScraperService> logger)
        {
            _logger = logger;
        }

        public async Task<Stream> GetCsvStream(string url)
        {
            _logger.LogInformation($"Trying to get stream from {url}");

            try
            {
                var httpClient = new HttpClient();

                var httpResponseMessage = await httpClient.GetAsync(new Uri(url));

                httpResponseMessage.EnsureSuccessStatusCode();

                var response = await httpResponseMessage.Content.ReadAsStreamAsync();

                _logger.LogInformation($"Stream retrieved successfully");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<IDocument> GetDocumentAsync()
        {
            var context = CreateContext();

            var document = await context.OpenAsync(ScraperConstants.FullAddress);

            _logger.LogInformation("Succesfully retrieved document to scrape");

            return document;
        }

        public List<string> GetDownloadLinksFromDocument(IDocument document)
        {
            _logger.LogInformation("Scraping document");

            var lastTwoRows = ParsingHelper.RetrieveLastTwoRows(document);

            var downloadLinks = RetrieveCsvDownloadLinks(lastTwoRows);

            _logger.LogInformation($"Succesfully retrieved {downloadLinks.Count} download links");

            return downloadLinks;
        }

        private static List<string> RetrieveCsvDownloadLinks(IEnumerable<IElement> lastTwoRows)
        {
            var downloadButtons = lastTwoRows.Select(row => row.GetElementsByClassName(ScraperConstants.DownloadButtonClass));

            var listOfLinks = downloadButtons.Select(buttonElement => buttonElement.Select(link => link.GetAttribute(ScraperConstants.HrefAttribute))).ToList();

            var downloadLinks = ParsingHelper.ParseListOfLinks(listOfLinks);

            return downloadLinks;
        }

        private static IBrowsingContext CreateContext()
        {
            var config = Configuration.Default.WithDefaultLoader();
            var context = BrowsingContext.New(config);
            return context;
        }
    }
}