using CsvHelper.Configuration.Attributes;

namespace Models.Models
{
    public class CsvEntry
    {
        [Name("TINKLAS")]
        public string NetworkName { get; set; }

        [Name("OBT_PAVADINIMAS")]
        public string OBTName { get; set; }

        [Name("OBJ_GV_TIPAS")]
        public string OBJGVType { get; set; }

        [Name("OBJ_NUMERIS")]
        public string OBJNumber { get; set; }

        [Name("P+")]
        public string? PPlus { get; set; }

        [Name("PL_T")]
        public string PLT { get; set; }

        [Name("P-")]
        public string? PMinus { get; set; }
    }
}