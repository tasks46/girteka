namespace Tests
{
    [TestFixture]
    public class ScraperTests
    {
        private readonly IScraperService _scraperService;

        public ScraperTests()
        {
            var logger = Mock.Of<ILogger<ScraperService>>();
            _scraperService = new ScraperService(logger);
        }

        [Test]
        public async Task ShouldGetCsvStream()
        {
            var stream = await _scraperService.GetCsvStream(TestsConstants.CsvDownloadUrl);

            Assert.That(stream, Is.Not.Null);
        }

        [Test]
        public async Task ShouldGetDocument()
        {
            var document = await _scraperService.GetDocumentAsync();

            Assert.That(document, Is.Not.Null);
        }

        [Test]
        public async Task ShouldGetLinksFromDocument()
        {
            var document = await _scraperService.GetDocumentAsync();

            Assert.That(document, Is.Not.Null);

            var links = _scraperService.GetDownloadLinksFromDocument(document);

            Assert.That(links, Is.Not.Null);
            Assert.That(links, Has.Count.EqualTo(2));
        }
    }
}