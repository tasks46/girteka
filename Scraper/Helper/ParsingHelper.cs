using AngleSharp.Dom;
using Scraper.Constants;

namespace Scraper.Helper
{
    public class ParsingHelper
    {
        public static List<string> ParseListOfLinks(List<IEnumerable<string>> listOfLinks)
        {
            var rawLinks = new List<string>();

            foreach (var linkListItem in listOfLinks)
            {
                rawLinks.Add(linkListItem.First());
            }

            return rawLinks;
        }

        public static IEnumerable<IElement> RetrieveLastTwoRows(IDocument document)
        {
            var table = document.GetElementById(ScraperConstants.TableID);

            if (table == null)
                throw new Exception("Table was not found, can't scrape");

            var tableRows = table.QuerySelectorAll(ScraperConstants.TableBodyRowClass);

            var lastTwoRows = tableRows.Skip(Math.Max(0, tableRows.Count() - 2));

            return lastTwoRows;
        }
    }
}