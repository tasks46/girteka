using Models.Models;

namespace Common.CsvParser
{
    public interface ICsvParser
    {
        List<CsvEntry> FilterButai(IList<CsvEntry> csvEntries);

        List<CsvEntry> GetCsvEntriesFromStream(Stream csvStream);

        List<string> GetNetworkNames(IList<CsvEntry> csvEntries);
    }
}