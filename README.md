Running locally:

1. Run a postgresdb in docker with command:
docker run --name postgres-docker -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres

2. Build and run the app itself.

3. Check console logs to see when scraping has finished.

4. Run Postman.
	4.1 Change settings to allow maximum response size to 0 (unlimited size)

Running in docker.

1. Run postgres-docker

1. Get postgres-docker db ip address with:
docker inspect postgres-docker -f "{{json .NetworkSettings.Networks }}"

2. In appsettings.json update "TestDbMaster" and "TestDb Server" values with the ip address retrieved from the docker inspect

3. Build and run the app in docker.

4. Check console logs to see when scraping has finished.

4. Run Postman.
	4.1 Change settings to allow maximum response size to 0 (unlimited size)
	
