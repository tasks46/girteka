using Repository.Service;

namespace api.Extensions
{
    public static class EndpointMapper
    {
        public static WebApplication MapEndpoints(this WebApplication app)
        {
            app.MapGet("/electricityData", async (IRepositoryService repositoryService) =>
            {
                var data = await repositoryService.RetrieveParsedDataEntriesAsync();

                if (data != null)
                    return Results.Ok(data);
                else
                    return Results.NotFound();
            })
            .WithName("GetElectricityData");

            return app;
        }
    }
}