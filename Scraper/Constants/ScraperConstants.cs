namespace Scraper.Constants
{
    public static class ScraperConstants
    {
        public static string BaseUrl = "https://data.gov.lt";
        public static string FullAddress = $"{BaseUrl}/dataset/siame-duomenu-rinkinyje-pateikiami-atsitiktinai-parinktu-1000-buitiniu-vartotoju-automatizuotos-apskaitos-elektriniu-valandiniai-duomenys";

        public static string TableID = "resource-table";
        public static string TableBodyRowClass = "tbody > tr";
        public static string DownloadButtonClass = "button is-link is-small is-pulled-right";
        public static string HrefAttribute = "href";
    }
}