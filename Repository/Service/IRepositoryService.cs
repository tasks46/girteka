using Models.Models;

namespace Repository.Service
{
    public interface IRepositoryService
    {
        Task<bool> InsertParsedDataEntriesAsync(List<CsvEntry> csvEntries);

        Task InsertNetworksAsync(List<string> networkNames);

        Task<IEnumerable<CsvEntry>> RetrieveParsedDataEntriesAsync();

        Task CleanDatabaseAsync();
    }
}