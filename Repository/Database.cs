using Dapper;
using Repository.Context;

namespace Repository
{
    public class Database
    {
        private readonly DapperContext _context;

        public Database(DapperContext context)
        {
            _context = context;
        }

        public void CreateDatabase(string dbName)
        {
            var query = $"SELECT datname FROM pg_catalog.pg_database WHERE lower(datname) = lower('{dbName}');";

            using (var connection = _context.CreateMasterConnection())
            {
                var records = connection.Query(query);
                if (!records.Any())
                    connection.Execute($"CREATE DATABASE {dbName}");
            }
        }
    }
}